import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class vPartsService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/vPartss';

  constructor(private http: HttpClient) { }

  getvParts(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createvParts(vParts: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, vParts);
  }

  updatevParts(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletevParts(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getvPartssList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
