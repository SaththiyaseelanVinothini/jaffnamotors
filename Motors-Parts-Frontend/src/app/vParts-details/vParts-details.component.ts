import { vParts } from '../vParts';
import { Component, OnInit, Input } from '@angular/core';
import { vPartsService } from '../vParts.service';
import { vPartsListComponent } from '../vParts-list/vParts-list.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vParts-details',
  templateUrl: './vParts-details.component.html',
  styleUrls: ['./vParts-details.component.css']
})
export class vPartsDetailsComponent implements OnInit {

  id: number;
  vParts: vParts;

  constructor(private route: ActivatedRoute,private router: Router,
    private vPartsService: vPartsService) { }

  ngOnInit() {
    this.vParts = new vParts();

    this.id = this.route.snapshot.params['id'];
    
    this.vPartsService.getvParts(this.id)
      .subscribe(data => {
        console.log(data)
        this.vParts = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['vPartss']);
  }
}
