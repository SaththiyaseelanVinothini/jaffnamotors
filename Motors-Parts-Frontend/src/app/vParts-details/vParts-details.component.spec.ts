import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { vPartsDetailsComponent } from './vParts-details.component';

describe('vPartsDetailsComponent', () => {
  let component: vPartsDetailsComponent;
  let fixture: ComponentFixture<vPartsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ vPartsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(vPartsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
