import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { vPartsListComponent } from './vParts-list.component';

describe('vPartsListComponent', () => {
  let component: vPartsListComponent;
  let fixture: ComponentFixture<vPartsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ vPartsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(vPartsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
