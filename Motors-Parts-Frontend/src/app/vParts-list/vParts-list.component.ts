import { vPartsDetailsComponent } from './../vParts-details/vParts-details.component';
import { Observable } from "rxjs";
import { vPartsService } from "../vParts.service";
import { vParts } from "../vParts";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-vParts-list",
  templateUrl: "./vParts-list.component.html",
  styleUrls: ["./vParts-list.component.css"]
})
export class vPartsListComponent implements OnInit {
  vPartss: Observable<vParts[]>;

  constructor(private vPartsService: vPartsService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.vPartss = this.vPartsService.getvPartssList();
  }

  deletevParts(id: number) {
    this.vPartsService.deletevParts(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  vPartsDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updatevParts(id: number){
    this.router.navigate(['update', id]);
  }
}
