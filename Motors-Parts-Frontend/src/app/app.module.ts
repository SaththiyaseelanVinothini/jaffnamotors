import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatevPartsComponent } from './create-vParts/create-vParts.component';
import { vPartsDetailsComponent } from './vParts-details/vParts-details.component';
import { vPartsListComponent } from './vParts-list/vParts-list.component';
import { HttpClientModule } from '@angular/common/http';
import { UpdatevPartsComponent } from './update-vParts/update-vParts.component';
@NgModule({
  declarations: [
    AppComponent,
    CreatevPartsComponent,
    vPartsDetailsComponent,
    vPartsListComponent,
    UpdatevPartsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
