import { TestBed } from '@angular/core/testing';

import { vPartsService } from './vParts.service';

describe('vPartsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: vPartsService = TestBed.get(vPartsService);
    expect(service).toBeTruthy();
  });
});
