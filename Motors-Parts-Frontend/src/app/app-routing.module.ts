import { vPartsDetailsComponent } from './vParts-details/vParts-details.component';
import { CreatevPartsComponent } from './create-vParts/create-vParts.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { vPartsListComponent } from './vParts-list/vParts-list.component';
import { UpdatevPartsComponent } from './update-vParts/update-vParts.component';

const routes: Routes = [
  { path: '', redirectTo: 'vParts', pathMatch: 'full' },
  { path: 'vPartss', component: vPartsListComponent },
  { path: 'add', component: CreatevPartsComponent },
  { path: 'update/:id', component: UpdatevPartsComponent },
  { path: 'details/:id', component: vPartsDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
