import { Component, OnInit } from '@angular/core';
import { vParts } from '../vParts';
import { ActivatedRoute, Router } from '@angular/router';
import { vPartsService } from '../vParts.service';

@Component({
  selector: 'app-update-vParts',
  templateUrl: './update-vParts.component.html',
  styleUrls: ['./update-vParts.component.css']
})
export class UpdatevPartsComponent implements OnInit {

  id: number;
  vParts: vParts;

  constructor(private route: ActivatedRoute,private router: Router,
    private vPartsService: vPartsService) { }

  ngOnInit() {
    this.vParts = new vParts();

    this.id = this.route.snapshot.params['id'];
    
    this.vPartsService.getvParts(this.id)
      .subscribe(data => {
        console.log(data)
        this.vParts = data;
      }, error => console.log(error));
  }

  updatevParts() {
    this.vPartsService.updatevParts(this.id, this.vParts)
      .subscribe(data => {
        console.log(data);
        this.vParts = new vParts();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updatevParts();    
  }

  gotoList() {
    this.router.navigate(['/vPartss']);
  }
}
