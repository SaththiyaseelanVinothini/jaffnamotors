import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatevPartsComponent } from './update-vParts.component';

describe('UpdatevPartsComponent', () => {
  let component: UpdatevPartsComponent;
  let fixture: ComponentFixture<UpdatevPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatevPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatevPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
