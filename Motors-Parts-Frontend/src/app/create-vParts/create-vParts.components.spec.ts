import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatevPartsComponent } from './create-vParts.component';

describe('CreatevPartsComponent', () => {
  let component: CreatevPartsComponent;
  let fixture: ComponentFixture<CreatevPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatevPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatevPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
