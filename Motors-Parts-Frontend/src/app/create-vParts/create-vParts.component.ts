import { vPartsService } from '../vParts.service';
import { vParts } from '../vParts';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-vParts',
  templateUrl: './create-vParts.component.html',
  styleUrls: ['./create-vParts.component.css']
})
export class CreatevPartsComponent implements OnInit {

  vParts: vParts = new vParts();
  submitted = false;

  constructor(private vPartsService: vPartsService,
    private router: Router) { }

  ngOnInit() {
  }

  newvParts(): void {
    this.submitted = false;
    this.vParts = new vParts();
  }

  save() {
    this.vPartsService
    .createvParts(this.vParts).subscribe(data => {
      console.log(data)
      this.vParts = new vParts();
      this.gotoList();
    }, 
    error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/vPartss']);
  }
}
