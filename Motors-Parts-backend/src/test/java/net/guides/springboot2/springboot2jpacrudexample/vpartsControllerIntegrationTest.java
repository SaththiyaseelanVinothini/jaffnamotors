package net.guides.springboot2.springboot2jpacrudexample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import net.guides.Backend.test.Demo;
import net.guides.Backend.test.model.Vparts;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Demo.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class vpartsControllerIntegrationTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testGetAllvpartss() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/vpartss",
				HttpMethod.GET, entity, String.class);
		
		assertNotNull(response.getBody());
	}

	@Test
	public void testGetvpartsById() {
		Vparts vparts = restTemplate.getForObject(getRootUrl() + "/vpartss/1", Vparts.class);
		System.out.println(vparts.getpartsname());
		assertNotNull(vparts);
	}

	@Test
	public void testCreatevparts() {
		Vparts vparts = new Vparts();
		vparts.setstock(15);
		vparts.setpartsname("tool");
		vparts.setshelveno("av002");

		ResponseEntity<Vparts> postResponse = restTemplate.postForEntity(getRootUrl() + "/vpartss", vparts, Vparts.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdatevparts() {
		int id = 1;
		Vparts vparts = restTemplate.getForObject(getRootUrl() + "/vpartss/" + id, Vparts.class);
		vparts.setpartsname("tool");
		vparts.setshelveno("av009");

		restTemplate.put(getRootUrl() + "/vpartss/" + id, vparts);

		Vparts updatedvparts = restTemplate.getForObject(getRootUrl() + "/vpartss/" + id, Vparts.class);
		assertNotNull(updatedvparts);
	}

	@Test
	public void testDeletevparts() {
		int id = 2;
		Vparts vparts = restTemplate.getForObject(getRootUrl() + "/vpartss/" + id, Vparts.class);
		assertNotNull(vparts);

		restTemplate.delete(getRootUrl() + "/vpartss/" + id);

		try {
			vparts = restTemplate.getForObject(getRootUrl() + "/vpartss/" + id, Vparts.class);
		} catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}
