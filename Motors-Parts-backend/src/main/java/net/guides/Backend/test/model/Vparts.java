package net.guides.Backend.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vparts")
public class Vparts {

	private Long partsid;
	private String partsname;
	private String shelveno;
	private int stock;
	
	public Vparts() {
		
	}
	
	public Vparts(String partsname, String shelveno, int stock) {
		this.partsname = partsname;
		this.shelveno = shelveno;
		this.stock = stock;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getpartsid() {
		return partsid;
	}
	public void setpartsid(long partsid) {
		this.partsid = partsid;
	}
	
	@Column(name = "partsname", nullable = false)
	public String getpartsname() {
		return partsname;
	}
	public void setpartsname(String partsname) {
		this.partsname = partsname;
	}
	
	@Column(name = "shelveno", nullable = false)
	public String getshelveno() {
		return shelveno;
	}
	public void setshelveno(String shelveno) {
		this.shelveno = shelveno;
	}
	
	@Column(name = "stock", nullable = false)
	public int stock() {
		return stock;
	}
	public void setstock( int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Vparts [partsid=" + partsid + ", partsname=" + partsname + ", shelveno=" + shelveno + ", stock=" + stock
				+ "]";
	}
	
}
