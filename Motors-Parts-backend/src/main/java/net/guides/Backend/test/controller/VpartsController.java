package net.guides.Backend.test.controller;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.Backend.test.exception.ResourceNotFoundException;
import net.guides.Backend.test.model.Vparts;
import net.guides.Backend.test.repository.VpartsRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class VpartsController {
	@Autowired
	private VpartsRepository VpartsRepository;

	@GetMapping("/Vparts")
	public List<Vparts> getAllEmployees() {
		return VpartsRepository.findAll();
	}

	@GetMapping("/vparts/{id}")
	public ResponseEntity<Vparts> getVpartsById(@PathVariable(value = "id") Long partsid)
			throws ResourceNotFoundException {
		Vparts Vparts = VpartsRepository.findById(partsid)
				.orElseThrow(() -> new ResourceNotFoundException("parts not found for this id :: " + partsid));
		return ResponseEntity.ok().body(Vparts);
	}

	@PostMapping("/vparts")
	public Vparts createVparts(@Valid @RequestBody Vparts Vparts) {
		return VpartsRepository.save(Vparts);
	}

	@PutMapping("/vparts/{id}")
	public ResponseEntity<Vparts> updateVparts(@PathVariable(value = "id") Long partsid,
			@Valid @RequestBody Vparts vpartsDetails) throws ResourceNotFoundException {
		Vparts vparts = VpartsRepository.findById(partsid)
				.orElseThrow(() -> new ResourceNotFoundException("Parts not found for this id :: " + partsid));

		vparts.setpartsid(vpartsDetails.getpartsid());
		vparts.setpartsname(vpartsDetails.getpartsname());
		vparts.setshelveno(vpartsDetails.getshelveno());
		final Vparts updatedvparts = VpartsRepository.save(vparts);
		return ResponseEntity.ok(updatedvparts);
	}

	@DeleteMapping("/Vparts/{id}")
	public Map<String, Boolean> deleteVparts(@PathVariable(value = "id") Long partsid)
			throws ResourceNotFoundException {
		Vparts vparts = VpartsRepository.findById(partsid)
				.orElseThrow(() -> new ResourceNotFoundException("Parts not found for this id :: " + partsid));

		VpartsRepository.delete(vparts);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
