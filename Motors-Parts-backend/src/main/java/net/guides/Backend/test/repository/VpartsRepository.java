package net.guides.Backend.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.guides.Backend.test.model.Vparts;


@Repository
public interface VpartsRepository extends JpaRepository <Vparts, Long>{

}
